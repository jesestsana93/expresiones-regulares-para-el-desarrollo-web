<?php
$error = [];
if(isset($_POST["nombre"])){
	$nombre = $_POST["nombre"];
	$anio = $_POST["anio"];
	$codpos = $_POST["codpos"];
	$email = $_POST["email"];
	$url = $_POST["url"];
	$salario = $_POST["salario"];
	$ip = $_POST["ip"];
	$fecha = $_POST["fecha"];
	$hora = $_POST["hora"];
	$clave = $_POST["clave"];
	####### Nombre #########
	$regex = "/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóúü]+[\s]*)+$/";
	if (!preg_match($regex, $nombre)) {
    	$error[0] = "El nombre no puede contener ni números ni caracteres especiales.";
	}
	####### Anio #########
	$regex = "/^(19[5-9]\d|20[0-4]\d)$/";
	if (!preg_match($regex, $anio)) {
    	$error[1]= "El año debe encontrarse entre 1950 y 2049.";
	}
	####### Código Postal #########
	$regex = "/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/";
	if (!preg_match($regex, $codpos)) {
    	$error[2]= "Formato del código postal es: ##### (cinco dígitos)";
	}
	####### Correo electrónico #########
	$regex = "/^[\w.%+\-]+@[\w.\-]+\.[A-Za-z]{2,7}$/";
	if (!preg_match($regex, $email)) {
    	$error[3]= "Formato de correo electrónico erróneo.";
	}
	####### URL #########
	$regex = "/^(http|https):\/\/[\w.\-]+(\.[\w\-]+)+[\w\-.,@?^~=%&:;+#]+$/";
	if (!preg_match($regex, $url)) {
    	$error[4]= "Formato http ó https. Debe contener '//www'. No puede escribir direcciones IP.";
	}
	####### Salario #########
	$regex = "/^(€|¥|£)?(\d*\.\d{0,2})$/";
	if (!preg_match($regex, $salario)) {
    	$error[5]= "Cantidad sin comas. Acepta dos decimales. Opcional símbolo de $, €, ¥, £";
	}
	####### ip #########
	$regex = "/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/";
	if (!preg_match($regex, $ip)) {
    	$error[6]= "Formato 255.255.255.255";
	}
	####### fecha #########
	$regex = "/^(19[5-9][0-9]|20[0-4][0-9])[-](0?[1-9]|1[0-2])[-](0?[1-9]|[12][0-9]|3[01])$/";
	if (!preg_match($regex, $fecha)) {
    	$error[7] = "Formato AAAA-MM-DD. El rango de los años es 1950 a 2049.";
	}
	####### hora #########
	$regex = "/^([0-1][0-9]|[2][0-3]):([0-5][0-9]):([0-5][0-9])$/";
	if (!preg_match($regex, $hora)) {
    	$error[8] = "Formato HH:MM:SS formato 24 hrs.";
	}
	####### Clave de acceso #########
	$regex = "/^(?=.*\d)(?=.*[~*+#!$%^&_\-\\=])(?=.*[A-Z])(?=.*[a-z])\S{8,15}$/";
	if (!preg_match($regex, $clave)) {
    	$error[9] = "No puede contener espacios. Mínimo 8 caracteres. Máximo 15 caracteres. Al menos una letra en mayúsculas. Al menos una letra en minúsculas. Al menos un número. Al menos un caracter especial o símbolo";
	}
}
function mensaje($cadena){
	print "<tr><td></td><td class='rojo'>".$cadena."</td></tr>";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Validación con expresiones regulares</title>
	<meta charset="utf-8">
	<style>
		body{ width:600px; margin:0 auto; }
		.correcto{ background: green; }
		.incorrecto{ background: red; }
		.rojo{ color:red; }
		input{ width:300px; }
	</style>
</head>

<body>
<fieldset>
<legend>Captura los siguientes datos</legend>
<form id="forma" action="forma.php" method="post">
	<table>
		<tr>
			<td>Nombre:</td>
			<td><input type="text" name="nombre" id="nombre" <?php print "value='".$nombre."'"; ?>/></td>
		</tr>
		<?php if(isset($error[0])) mensaje($error[0]);?>
		<tr>
			<td>Año de nacimiento:</td>
			<td><input type="text" name="anio" id="anio" <?php print "value='".$anio."'"; ?>/></td>
		</tr>
		<?php if(isset($error[1])) mensaje($error[1]);?>
		<tr>
			<td>Código postal:</td>
			<td><input type="text" name="codpos" id="codpos" <?php print "value='".$codpos."'"; ?>/></td>
		</tr>
		<?php if(isset($error[2])) mensaje($error[2]);?>
		<tr>
			<td>Correo electrónico:</td>
			<td><input type="text" name="email" id="email" <?php print "value='".$email."'"; ?>/></td>
		</tr>
		<?php if(isset($error[3])) mensaje($error[3]);?>
		<tr>
			<td>URL:</td>
			<td><input type="text" name="url" id="url" <?php print "value='".$url."'"; ?>/></td>
		</tr>
		<?php if(isset($error[4])) mensaje($error[4]);?>
		<tr>
			<td>Salario:</td>
			<td><input type="text" name="salario" id="salario" <?php print "value='".$salario."'"; ?>/></td>
		</tr>
		<?php if(isset($error[5])) mensaje($error[5]);?>
		<tr>
			<td>Dirección IP:</td>
			<td><input type="text" name="ip" id="ip" <?php print "value='".$ip."'"; ?>/></td>
		</tr>
		<?php if(isset($error[6])) mensaje($error[6]);?>
		<tr>
			<td>Fecha de nacimiento:</td>
			<td><input type="text" name="fecha" id="fecha" <?php print "value='".$fecha."'"; ?>/></td>
		</tr>
		<?php if(isset($error[7])) mensaje($error[7]);?>
		<tr>
			<td>Hora actual:</td>
			<td><input type="text" name="hora" id="hora" <?php print "value='".$hora."'"; ?>/></td>
		</tr>
		<?php if(isset($error[8])) mensaje($error[8]);?>
		<tr>
			<td>Clave de acceso:</td>
			<td><input type="text" name="clave" id="clave" <?php print "value='".$clave."'"; ?>/></td>
		</tr>
		<?php if(isset($error[9])) mensaje($error[9]);?>
		<tr>
			<td></td>
			<td><input type="submit" value="Enviar"/></td>
		</tr>
	</table>
</form>
</fieldset>
</body>
</html>